Данный проект чтобы продемонстировать знание в spring boot и angular js

## Как запустить
* Отредактируйте build.gradle и application.properties с вашими данными MySql базы.
* Создайте базу данных
* ```gradle flywayMigrate```
* ```gradle run```
* Если проект не запускается попробуйте
* ```gradle clean```
* ```gradle installApp```
* ```gradle run```

* Откройте в браузере http://localhost:8080/

# Автор Азамат Бекмуратов