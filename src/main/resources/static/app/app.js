(function (angular) {
    angular.module("azamatSpringBoot.controllers", []);
    angular.module("azamatSpringBoot.services", []);
    var app = angular.module("azamatSpringBoot",
        [
            "ngRoute",
            "ngResource",
            "azamatSpringBoot.controllers",
            "azamatSpringBoot.services"
        ]);

    app.config(['$routeProvider', '$locationProvider', function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'pages/home.html',
                controller: 'AppController'
            })
            .when('/add', {
                templateUrl: 'pages/addUser.html',
                controller: 'AddUserController'
            })
            .when('/edit/:id', {
                templateUrl: 'pages/editUser.html',
                controller: 'EditUserController'
            })
            .otherwise({redirectTo: '/'});
    }]);

    app.constant('dateFormat', "yyyy-mm-dd");
}(angular));